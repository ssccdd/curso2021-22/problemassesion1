/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo6;

import es.uja.ssccdd.curso2122.problemassesion1.grupo6.Utils.RolJugador;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo6.Utils.VALOR_GENERACION;
import java.util.ArrayList;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo6.Utils.EQUIPOS_A_GENERAR;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo6.Utils.JUGADORES_A_GENERAR;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion1 {

    public static void main(String[] args) {

        // Variables aplicación
        ArrayList<Equipo> listaEquipos;
        ArrayList<Jugador> listaJugadores;

        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");

        // Inicializamos los equipos y la lista de jugadores disponibles
        listaEquipos = new ArrayList<>();
        for (int i = 0; i < EQUIPOS_A_GENERAR; i++) {
            listaEquipos.add(new Equipo(i + 1));
        }

        listaJugadores = new ArrayList<>();
        for (int i = 0; i < JUGADORES_A_GENERAR; i++) {
            int aleatorioRol = Utils.random.nextInt(VALOR_GENERACION);
            listaJugadores.add(new Jugador(i + 1, RolJugador.getRolJugador(aleatorioRol)));
        }

        // Cuerpo principal de ejecución
        for (Jugador jugador : listaJugadores) {
            boolean asignado = false;
            for (int i = 0; i < listaEquipos.size() && !asignado; i++) {
                if (listaEquipos.get(i).addJugador(jugador)) {
                    asignado = true;
                }
            }

            if (!asignado) {
                System.out.println("No se ha podido asignar al jugador: " + jugador);
            }

        }

        // Mostrar la información de la lista de equipos
        System.out.println("(HILO_PRINCIPAL) Lista de equipos");
        for (Equipo equipo : listaEquipos) {
            System.out.println(equipo);
        }

        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }

}
