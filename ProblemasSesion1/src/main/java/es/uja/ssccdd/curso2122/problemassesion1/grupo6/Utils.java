/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo6;

import java.util.Random;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {
    
    public static Random random = new Random();
    
    // Constantes del problema
    public static final int EQUIPOS_A_GENERAR = 10;
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_ROLES = RolJugador.values().length;
    public static final int JUGADORES_POR_EQUIPO = 4;
    public static final int LIMITE_ROLES_POR_EQUIPO = 2;
    public static final int JUGADORES_A_GENERAR = 40;
    
    //Enumerado para el rol de jugador
    public enum RolJugador {
        ATAQUE(40), DEFENSA(80), POLIVALENTE(100);

        private final int valor;

        private RolJugador(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un rol relacionado con su valor de generación
         *
         * @param valor, entre 0 y 100, de generación
         * @return el rol asociado con el valor de generación
         */
        public static RolJugador getRolJugador(int valor) {
            RolJugador resultado = null;
            RolJugador[] roles = RolJugador.values();
            int i = 0;

            while ((i < roles.length) && (resultado == null)) {
                if (roles[i].valor >= valor) {
                    resultado = roles[i];
                }

                i++;
            }

            return resultado;
        }
    }
    
}
