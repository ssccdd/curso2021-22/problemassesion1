/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo1;

/**
 *
 * @author pedroj
 */
public class Persona {
    private final String nombre;
    private final int recursos;

    public Persona(String nombre, int recursos) {
        this.nombre = nombre;
        this.recursos = recursos;
    }

    public String getNombre() {
        return nombre;
    }

    public int getRecursos() {
        return recursos;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", recursos=" + recursos + '}';
    }
}
