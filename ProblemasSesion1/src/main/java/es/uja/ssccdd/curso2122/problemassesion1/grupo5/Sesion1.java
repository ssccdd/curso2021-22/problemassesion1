/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo5;

import static es.uja.ssccdd.curso2122.problemassesion1.grupo4.Utils.VALOR_GENERACION;
import es.uja.ssccdd.curso2122.problemassesion1.grupo5.Utils.TipoReserva;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo5.Utils.RESERVAS_A_GENERAR;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo5.Utils.COCHES_A_GENERAR;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo5.Utils.MAXIMO_COCHES_POR_RESERVA;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion1 {

    public static void main(String[] args) {

        // Variables aplicación
        ArrayList<Reserva> listaReservas;
        ArrayList<Coche> cochesPreparados;

        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");

        // Inicializamos las impresoras y la lista de modelos
        listaReservas = new ArrayList<>();
        for (int i = 0; i < RESERVAS_A_GENERAR; i++) {
            int aleatorioTipo = Utils.random.nextInt(VALOR_GENERACION);
            int aleatorioNCoches = Utils.random.nextInt(MAXIMO_COCHES_POR_RESERVA) + 1;
            listaReservas.add(new Reserva(i + 1, aleatorioNCoches ,TipoReserva.getTipoReserva(aleatorioTipo)));
        }

        cochesPreparados = new ArrayList<>();
        int siguienteReserva = 0;

        // Cuerpo principal de ejecución
        for (int i = 0; i < COCHES_A_GENERAR; i++) {
            int aleatorioCalidad = Utils.random.nextInt(VALOR_GENERACION);
            Coche coche = new Coche(i + 1, TipoReserva.getTipoReserva(aleatorioCalidad));
            cochesPreparados.add(coche);

            int reservasComprobadas = 0; //Para evitar bucles infinitos
            boolean encolado = false;
            while (!encolado && reservasComprobadas < RESERVAS_A_GENERAR) {// Si no se ha generado una impresora de un tipo, esto evita el bucle infinito

                if (listaReservas.get(siguienteReserva).addCoche(coche)) {
                    encolado = true;
                }

                reservasComprobadas++;
                siguienteReserva = (siguienteReserva + 1) % RESERVAS_A_GENERAR;

            }
        }

        // Mostrar la información de la lista de reservas
        System.out.println("(HILO_PRINCIPAL) Lista de reservas");
        for (Reserva reserva : listaReservas) {
            System.out.println(reserva);
        }

        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }

}
