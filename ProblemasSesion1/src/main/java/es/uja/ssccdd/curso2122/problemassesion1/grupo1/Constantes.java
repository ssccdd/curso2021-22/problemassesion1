/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo1;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    // Generador aleatorio
    public static final Random aleatorio = new Random();
    
    public enum TipoProducto {
        ALIMENTACION(20,2), OCIO(40,10), ROPA(60,5), CALZADO(80,2), VIAJES(100,7);
        
        private final int valor;
        private final int coste;

        private TipoProducto(int valor, int coste) {
            this.valor = valor;
            this.coste = coste;
        }
        
        /**
         * Genera aleatoriamente uno de los tipos de producto disponibles
         * @return el tipo de producto generado
         */
        public static TipoProducto getTipoProducto() {
            TipoProducto resultado = null;
            int valor = aleatorio.nextInt(D100);
            
            int i = 0;
            
            while( (i < productos.length) && (resultado == null) ) {
                if( productos[i].valor > valor)
                    resultado = productos[i];
                
                i++;
            }
            return resultado;
        }

        /**
         * Indica el coste para ese tipo de producto
         * @return el coste
         */
        public int getCoste() {
            return coste;
        }
    }
    
    // Constantes de la sesión
    public static final int D100 = 100; // Tirada de dato de 100 caras
    public static final TipoProducto[] productos = TipoProducto.values();
    public static final int MIN_RECURSOS = 40;
    public static final int INCREMENTO_RECURSOS = 21;
    public static final int TOTAL_PEDIDOS = 10;
    public static final int MAX_DESCARTADOS = 10;
    public static final int PRIMERO = 0;
    public static final boolean NO_ASIGNADO = false;
    public static final boolean ASIGNADO = true;
    public static final int NINGUNO = 0;
}