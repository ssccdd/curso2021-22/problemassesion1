/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo6;

import static es.uja.ssccdd.curso2122.problemassesion1.grupo6.Utils.RolJugador.ATAQUE;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo6.Utils.JUGADORES_POR_EQUIPO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo6.Utils.LIMITE_ROLES_POR_EQUIPO;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Equipo {

    private final int iD;
    private ArrayList<Jugador> jugadores;

    /**
     * Genera un nuevo equipo
     *
     * @param iD id del paciente
     */
    public Equipo(int iD) {
        this.iD = iD;
        this.jugadores = new ArrayList<>();
    }

    public int getiD() {
        return iD;
    }

    public int getNumeroJugadores() {
        return jugadores.size();
    }

    /**
     * Asigna un nuevo jugador al equipo
     *
     * @param jugador para añadir a la lista
     * @return true si se ha añadido false si no es posible
     */
    public boolean addJugador(Jugador jugador) {
        boolean resultado = false;
        int nAtacantes = 0;
        int nMismoRol = 0;
        if (jugadores.size() < JUGADORES_POR_EQUIPO) {
            for (Jugador jug : jugadores) {
                if (jug.getRol().equals(jugador.getRol())) {
                    nMismoRol++;
                }
                if (jug.getRol().equals(ATAQUE)) {
                    nAtacantes++;
                }
            }

            if (nAtacantes == 0 && jugadores.size() == JUGADORES_POR_EQUIPO - 1) {
                //Solo queda un hueco y no hay atacantes
                if (jugador.getRol().equals(ATAQUE)) {
                    resultado = true;
                    jugadores.add(jugador);
                }
                //Si no soy atacante no se puede introducir porque hay que dejar espacio para el atacante.
            } else {
                if (nMismoRol<LIMITE_ROLES_POR_EQUIPO) {
                    resultado = true;
                    jugadores.add(jugador);
                }
            }
        }
        
        return resultado;
    }
    
    public boolean isCompleto(){
        return jugadores.size() == JUGADORES_POR_EQUIPO;
    }

    @Override
    public String toString() {
        StringBuilder resultado = new StringBuilder();

        resultado.append("Equipo[" + iD + ", completo='" + (isCompleto() ? "SI" : "NO") + "']{" + "Jugadores=[");

        for (Jugador jugador : jugadores) {
            resultado.append(jugador.toString() + ", ");
        }

        resultado.delete(resultado.length() - 2, resultado.length()); // para borrar la última coma
        resultado.append("]}");

        return resultado.toString();
    }

}
