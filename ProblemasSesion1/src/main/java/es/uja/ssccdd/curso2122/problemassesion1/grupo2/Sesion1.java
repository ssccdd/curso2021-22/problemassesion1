/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.PRIMERO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.D100;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.FIN_PROCESO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.MIN_PAGINAS;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.TOTAL_PROCESOS;
import es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.VARIACION_PAGINAS;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author pedroj
 */
public class Sesion1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GestorMemoria gestor;
        Proceso[] procesosCreados;
        ArrayList<Proceso> procesosRechazados;
        ArrayList<Proceso> finalizados;
        Proceso proceso;
        int procesoAsignado;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las variables del sistema
        gestor = new GestorMemoria("GestorMemoria", MIN_PAGINAS + aleatorio.nextInt(VARIACION_PAGINAS));
        procesosRechazados = new ArrayList();
        finalizados = new ArrayList();
        procesosCreados = new Proceso[TOTAL_PROCESOS];
        procesoAsignado = PRIMERO;
        for(int i = 0; i < TOTAL_PROCESOS; i++) {
            proceso = new Proceso("iD(" + i + ")", TipoProceso.getTipoProceso());
            procesosCreados[i] = proceso;
        }
        
        // Cuerpo de ejecución del hilo principal
        do{
            if( !gestor.addProceso(procesosCreados[procesoAsignado]) )
                procesosRechazados.add(procesosCreados[procesoAsignado]);
            
            if( aleatorio.nextInt(D100) > FIN_PROCESO )
                finalizados.add(gestor.removeProceso());
            
            procesoAsignado++;
            
        } while( !gestor.ejecucionCompletada() && (procesoAsignado < TOTAL_PROCESOS) );
        
        // Presentar resultados
        System.out.println(gestor);
        System.out.println("Procesos creados\n\t" + Arrays.toString(procesosCreados));
        System.out.println("Procesos finalizados\n\t" + finalizados);
        System.out.println("Procesos rechazados\n\t" + procesosRechazados);
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    } 
}
