/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.INCREMENTO_RECURSOS;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.MAX_DESCARTADOS;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.MIN_RECURSOS;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.NINGUNO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.NO_ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.PRIMERO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.TOTAL_PEDIDOS;
import es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.TipoProducto;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.aleatorio;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Sesion1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pedido[] listaPedidos;
        Persona cliente;
        int recursos;
        ArrayList<TipoProducto> productosDescartados;
        TipoProducto producto;
        int ultimoRevisado;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");

        // Inicializamos las variables del sistema
        ultimoRevisado = PRIMERO;
        productosDescartados = new ArrayList();
        listaPedidos = new Pedido[TOTAL_PEDIDOS];
        for(int i = 0; i < TOTAL_PEDIDOS; i++) {
            recursos = MIN_RECURSOS + aleatorio.nextInt(INCREMENTO_RECURSOS);
            cliente = new Persona("Cliente-" + i, recursos);
            listaPedidos[i] = new Pedido("Pedido-" + i, cliente);
        }
        
        // Cuerpo de ejecución del hilo principal
        do{
            boolean asignado = NO_ASIGNADO;
            int pedidosRevisados = NINGUNO;
            producto = TipoProducto.getTipoProducto();
            
            // Se busca un pedido donde asignar el producto empezando por el último
            // que se revisó previamente
            while( (pedidosRevisados < TOTAL_PEDIDOS) && !asignado ) {
                ultimoRevisado = (ultimoRevisado + pedidosRevisados) % TOTAL_PEDIDOS;
                if( listaPedidos[ultimoRevisado].addProducto(producto) ) 
                    asignado = ASIGNADO;
                else
                    pedidosRevisados++;
            }
            
            if( !asignado )
                productosDescartados.add(producto);
            
        } while (productosDescartados.size() < MAX_DESCARTADOS);
        
        // Presentar resultados
        System.out.println("Lista de Pedidos___________________________________");
        for(Pedido pedido : listaPedidos)
            System.out.println(pedido);
        System.out.println("Productos descartados______________________________");
        System.out.println(productosDescartados);
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
