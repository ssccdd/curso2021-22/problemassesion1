/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo4;

import es.uja.ssccdd.curso2122.problemassesion1.grupo4.Utils.TipoRecurso;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Recurso{ 
    
    private final int iD;
    private final TipoRecurso tipo;
    private final int creditos;

    public Recurso(int iD, TipoRecurso tRecurso, int creditos) {
        this.iD = iD;
        this.tipo = tRecurso;
        this.creditos = creditos;
    }

    public int getiD() {
        return iD;
    }

    public TipoRecurso getTipo() {
        return tipo;
    }

    public int getCreditos() {
        return creditos;
    }

    @Override
    public String toString() {
        return "Recurso{" + "iD= " + iD + ", tipo de recurso= " + tipo + ", créditos= " + creditos + '}';
    }
    
}
