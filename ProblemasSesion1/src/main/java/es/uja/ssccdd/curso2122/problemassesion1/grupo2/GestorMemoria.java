/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.NO_ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.PRIMERO;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class GestorMemoria {
    private final String ID;
    private final int paginasAsignadas;
    private final ArrayList<Proceso> listaProcesos;
    private int paginasOcupadas;

    public GestorMemoria(String ID, int paginasAsignadas) {
        this.ID = ID;
        this.paginasAsignadas = paginasAsignadas;
        this.listaProcesos = new ArrayList();
        this.paginasOcupadas = 0;
    }

    public String getID() {
        return ID;
    }

    public int getPaginasAsignadas() {
        return paginasAsignadas;
    }

    public int getPaginasOcupadas() {
        return paginasOcupadas;
    }

    public ArrayList<Proceso> getListaProcesos() {
        return listaProcesos;
    }
    
    public boolean addProceso(Proceso proceso) {
        boolean resultado = NO_ASIGNADO;
        
        if( (proceso.getPaginas() + paginasOcupadas) <= paginasAsignadas ) {
            paginasOcupadas = paginasOcupadas + proceso.getPaginas();
            listaProcesos.add(proceso);
            resultado = ASIGNADO;
        }
        
        return resultado;
    }
    
    public Proceso removeProceso() {
        
        Proceso proceso = listaProcesos.remove(PRIMERO);
        paginasOcupadas = paginasOcupadas - proceso.getPaginas();
        
        return proceso;
    }
    
    public boolean ejecucionCompletada() {
        return listaProcesos.isEmpty();
    }

    @Override
    public String toString() {
        return "GestorMemoria{" + 
               "\n\tID = " + ID + 
               "\n\tPaginas Asignadas = " + paginasAsignadas + 
               "\n\tPaginas Ocupadas = " + paginasOcupadas +
               "\n\tLista Procesos\n\t\t" + listaProcesos +
               "\n}";
    }  
}
