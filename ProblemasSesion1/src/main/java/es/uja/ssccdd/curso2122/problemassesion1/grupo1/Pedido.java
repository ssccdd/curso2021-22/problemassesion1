/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.NO_ASIGNADO;
import es.uja.ssccdd.curso2122.problemassesion1.grupo1.Constantes.TipoProducto;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Pedido {
    private final String ID;
    private final Persona comprador;
    private final ArrayList<TipoProducto> listaProductos;
    private int coste;

    public Pedido(String ID, Persona comprador) {
        this.ID = ID;
        this.comprador = comprador;
        this.listaProductos = new ArrayList();
        this.coste = 0;
    }

    public String getID() {
        return ID;
    }

    public Persona getComprador() {
        return comprador;
    }

    public int getCoste() {
        return coste;
    }

    public ArrayList<TipoProducto> getListaProductos() {
        return listaProductos;
    }
    
    public boolean addProducto(TipoProducto producto) {
        boolean resultado = NO_ASIGNADO;
        
        if( (producto.getCoste() + coste) <= comprador.getRecursos() ) {
            coste = coste + producto.getCoste();
            listaProductos.add(producto);
            resultado = ASIGNADO;
        }
        
        return resultado;
    }

    @Override
    public String toString() {
        return "Pedido{" +
               "\n\tID = " + ID +
               "\n\tComprador = " + comprador +
               "\n\tCoste Pedido = " + coste +
               "\n\tProductos = " + listaProductos +
               "\n}";   
    }    
}
