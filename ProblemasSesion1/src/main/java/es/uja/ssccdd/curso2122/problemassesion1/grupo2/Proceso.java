/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo2;

import es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo2.Constantes.aleatorio;

/**
 *
 * @author pedroj
 */
public class Proceso {
    private final String ID;
    private final TipoProceso tipoProceso;
    private final int paginas;

    public Proceso(String ID, TipoProceso tipoProceso) {
        this.ID = ID;
        this.tipoProceso = tipoProceso;
        this.paginas = aleatorio.nextInt(tipoProceso.getMaximoPaginas()) + 1;
    }

    public String getID() {
        return ID;
    }

    public TipoProceso getTipoProceso() {
        return tipoProceso;
    }

    public int getPaginas() {
        return paginas;
    }

    @Override
    public String toString() {
        return "Proceso{" + "ID=" + ID + ", tipoProceso=" + tipoProceso + 
               ", paginas=" + paginas + '}';
    }
}
