/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo6;

import es.uja.ssccdd.curso2122.problemassesion1.grupo6.Utils.RolJugador;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Jugador {
    
    private final int iD;
    private final RolJugador rol;

    public Jugador(int iD, RolJugador rol) {
        this.iD = iD;
        this.rol = rol;
    }

    public int getiD() {
        return iD;
    }

    public RolJugador getRol() {
        return rol;
    }

    @Override
    public String toString() {
        return "Jugador{" + "iD= " + iD + ", rol= " + rol + "}";
    }
    
}
