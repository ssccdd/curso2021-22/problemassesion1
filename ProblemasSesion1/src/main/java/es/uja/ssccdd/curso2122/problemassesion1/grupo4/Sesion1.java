/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo4;

import es.uja.ssccdd.curso2122.problemassesion1.grupo4.Utils.TipoRecurso;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo4.Utils.PLANESTUDIOS_A_GENERAR;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo4.Utils.RECURSOS_A_GENERAR;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo4.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo4.Utils.CREDITOS_MAXIMOS_ASIGNATURA;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo4.Utils.CREDITOS_MINIMOS_ASIGNATURA;


/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion1 {


    public static void main(String[] args) {

        // Variables aplicación
        PlanEstudios[] listaPlanes;
        Recurso[] listaRecursos;

        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");

        // Inicializamos los menús y la lista de platos para los pedidos
        listaPlanes = new PlanEstudios[PLANESTUDIOS_A_GENERAR];
        for (int i = 0; i < PLANESTUDIOS_A_GENERAR; i++) {
            listaPlanes[i] = new PlanEstudios(i + 1);
        }

        listaRecursos = new Recurso[RECURSOS_A_GENERAR];
        for (int i = 0; i < RECURSOS_A_GENERAR; i++) {
            int aleatorioRecurso = Utils.random.nextInt(VALOR_GENERACION);
            int aleatorioCreditos = Utils.random.nextInt(CREDITOS_MAXIMOS_ASIGNATURA - CREDITOS_MINIMOS_ASIGNATURA) + CREDITOS_MINIMOS_ASIGNATURA;
            listaRecursos[i] = new Recurso(i + 1, TipoRecurso.getRecurso(aleatorioRecurso), aleatorioCreditos);
        }

        // Cuerpo principal de ejecución
        int i = 0;
        while (i < RECURSOS_A_GENERAR) {
            
            int j = 0;
            boolean insertado = false;
            while( j < PLANESTUDIOS_A_GENERAR && !insertado){
                if(listaPlanes[j].addRecurso(listaRecursos[i]))
                    insertado=true;
                j++;
            }
            
            if(!insertado)
                System.out.println("El recurso " + listaRecursos[i] + " no ha podido asignarse a ningún plan.");
            
            i++;
        }

        // Mostrar la información de la lista de pedidos
        System.out.println("(HILO_PRINCIPAL) Lista de planes de estudios");
        for (PlanEstudios planes : listaPlanes) {
            System.out.println(planes);
        }

        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }

}

