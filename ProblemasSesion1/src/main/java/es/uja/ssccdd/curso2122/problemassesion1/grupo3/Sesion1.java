/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion1.grupo3;

import static es.uja.ssccdd.curso2122.problemassesion1.grupo3.Constantes.ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo3.Constantes.MIN_COMPONENTES;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo3.Constantes.PRIMERO;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo3.Constantes.TOTAL_ORDENADORES;
import es.uja.ssccdd.curso2122.problemassesion1.grupo3.Constantes.TipoComponente;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo3.Constantes.VARIACION_COMPONENTES;
import static es.uja.ssccdd.curso2122.problemassesion1.grupo3.Constantes.aleatorio;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Sesion1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Ordenador[] listaOrdenadores;
        ArrayList<Componente> componentesDescartados;
        Componente[] listaComponentes;
        
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las variables del sistema
        componentesDescartados = new ArrayList();
        listaOrdenadores = new Ordenador[TOTAL_ORDENADORES];
        for(int i = 0; i < TOTAL_ORDENADORES; i++) 
            listaOrdenadores[i] = new Ordenador("Ordenador-" + i);
        listaComponentes = new Componente[MIN_COMPONENTES + aleatorio.nextInt(VARIACION_COMPONENTES)];
        for(int i = 0; i < listaComponentes.length; i++)
            listaComponentes[i] = new Componente("Componente-" + i, TipoComponente.getTipoComponente());
        
        // Cuerpo de ejecución del hilo principal
        for(Componente componente : listaComponentes) {
            boolean asignado = !ASIGNADO;
            int orden = PRIMERO;
            
            while( (orden < TOTAL_ORDENADORES) && !asignado ) 
                if( listaOrdenadores[orden].addComponente(componente) )
                    asignado = ASIGNADO;
                else
                    orden++;
            
            if( !asignado )
                componentesDescartados.add(componente);
        }
        
        // Presentar resultados
        System.out.println("Lista de ordenadores _____________________________________");
        for(Ordenador ordenador : listaOrdenadores)
            System.out.println(ordenador);
        System.out.println("Componentes descartados __________________________________");
        System.out.println(componentesDescartados);
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    } 
}
