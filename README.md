[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 1
Problemas propuestos para la Sesión 1 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2021-22.

Los **objetivos de la práctica** son:
- Definir adecuadamente las clases que se piden en la práctica.
- Modificar el método `main(..)` de la aplicación Java para una prueba correcta de las clases previamente definidas.

Los ejercicios son diferentes para cada grupo:
- [Grupo 1](https://gitlab.com/ssccdd/curso2021-22/problemassesion1/-/blob/master/README.md#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2021-22/problemassesion1/-/blob/master/README.md#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2021-22/problemassesion1/-/blob/master/README.md#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2021-22/problemassesion1/-/blob/master/README.md#grupo-4)
- [Grupo 5](https://gitlab.com/ssccdd/curso2021-22/problemassesion1/-/blob/master/README.md#grupo-5)
- [Grupo 6](https://gitlab.com/ssccdd/curso2021-22/problemassesion1/-/blob/master/README.md#grupo-6)

### Grupo 1

Se deberán completar una serie de pedidos para unos clientes que tendrán una disponibilidad de recursos limitada. Para ello se deberán realizar las siguientes tareas:

-   Añadir todas las constantes necesarias en la interface  **Constantes** para la resolución del ejercicio.
-   Definirla una clase  **Persona** que tendrá un identificador, nombre y cantidad de recursos. Deberá implementarse el método  `toString()`  para dar una representación de un objeto de la clase.
-   Definir una clase  **Pedido** que estará asociado a una persona y que estará compuesto por una lista de los tipos de producto que lo componen. Tendrá que tener un método que permita añadir un  **TipoProducto**  al pedido si no sobrepasa la capacidad de recursos de la persona asociada. También hay que dar una implementación del método  `toString()`.
-   **HiloPrincipal**:
    -   Se crearán 10 pedidos asociados a 10 personas con recursos variables entre 40 y 60.
    -   Hacer
        -   Generar un producto aleatorio.
        -   Encontrar un pedido donde asignarlo.
        -   Si no se ha encontrado un pedido donde asignarlo guardarlo en una lista de productos descartados.
        -   Repetir esta acción hasta que la lista de descartados alcance un total de 10 productos.
    -   Antes de finalizar presentar la lista de pedidos y de productos descartados.

### Grupo 2

En un sistema informático los diferentes procesos que se crean deberán almacenarse en memoria para su correcto funcionamiento.

Para la realización del ejercicio propuesto se deben crear las siguientes clases:

-   Añadir todas las constantes necesarias en la interface  **Constantes**  para la resolución del ejercicio.
-   Definir la clase  **Proceso**  que tendrá un identificador único, un  **TipoProceso**. Cuando se cree el proceso deberá generarse un número de páginas aleatorio que no podrá exceder el máximo permitido para ese tipo de proceso. Deberá implementarse el método  `toString()`  para tener una representación del proceso.
-   Definir la clase  **GestorMemoria**  que tendrá un número máximo de páginas de memoria asignadas y una lista de procesos que están bajo su cargo. Se deberán definir métodos para:
    -   Asignar un proceso a la lista de procesos que gestiona, no podrá añadirse el proceso si excede las páginas que tiene disponibles en ese momento el gestor.
    -   Eliminar uno de los procesos que controla el gestor.
    -   Saber si se ha completado la ejecución de los procesos asignados al gestor.
    -   También hay que dar una implementación para el método  `toString()`.
-   **HiloPrincipal**:
    -   Se crea un gestor de memoria con una capacidad aleatoria de páginas entre 20 y 40.
    -   Se crea una lista de 30 procesos, cada proceso tendrá un tipo de proceso aleatorio.
    -   Hacer:
        -   Asignar un proceso de la lista al gestor. Si no se puede asignar al gestor se añadirá a una lista de procesos rechazados.
        -   Con una probabilidad del 50% se eliminará uno de los procesos del gestor porque ha completado su ejecución.
        -   Hasta que el gestor no tenga procesos asignados o no haya procesos disponibles para su asignación.
    -   Antes de finalizar presentar el estado del gestor, la lista de procesos rechazados y la lista de procesos creados.

### Grupo 3

Tenemos un proveedor que deberá componer diferentes ordenadores que tendrán a su disposición una serie de componentes.

Para la realización del ejercicio propuesto se deben crear las siguientes clases:

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio.
-   Definir la clase  **Componente** que tendrá un identificador único, un **TipoComponente**. Deberá implementarse el método `toString()` para tener una representación del componente.
-   Definir la clase  **Ordenador** tendrá un identificador único, una lista de componentes y cuando se cree se decidirá aleatoriamente si es simple o tendrá el doble de componentes mínimos necesarios. Se deberán definir métodos para:
    -   Asignar un componente y indicará si ya se ha alcanzado el límite para ese componente para la configuración del ordenador.
    -   Saber si el ordenador está finalizado.
    -   También hay que dar una implementación para el método `toString()`.
-   **HiloPrincipal**:
    -   Se crea una lista de entre 30 y 60 componentes aleatorios.
    -   Se crea una lista de 10 ordenadores que se deberán completar.
    -   Mientras haya componentes en la lista
        -   Buscar un ordenador donde asignar ese componente.
        -   Si no se encuentra ningún ordenador donde asignarlo se añade a una lista de componentes descartados.
    -   Antes de finalizar presentar la lista de ordenadores y la lista de componentes descartados.

### Grupo 4
En una plataforma online de educación los recursos de aprendizaje online se asignan a diferentes planes de estudios.

Para la realización del ejercicio propuesto se deben crear las siguientes clases:
- `Recurso` : Identificará a un recurso de aprendizaje por medio de un número de identificación. También tendrá asociado un `TipoRecurso` que identifica si es una asignatura, un foro o un blog y un número entero de créditos. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
- `PlanEstudios`: Identificará a un plan de estudios por medio de un número de identificación. Como máximo puede tener un recurso de tipo blog y foro, no puede tener mas de un foro, ni mas de un blog, pero si un foro y un blog a la vez. Y la suma de créditos de las asignaturas no puede ser mayor a 8.
	- Hay que definir el constructor y los métodos de acceso correspondientes.
	- Definir el método `toString()` para mostrar los datos del plan con los datos de cada recurso.
- `Hilo Principal`:
	- Generar una lista de 10 planes de estudio.
	- Generar 30 recursos:
		- Generar un valor de construcción aleatorio entre 0 y 100.
		- Generar un número de créditos entre 3 y 6.
		- Seleccionar el tipo de recurso correspondiente para el recurso con el valor de construcción generado.
	- Asignar cada recurso a uno de los planes, si el recurso no puede ser asignado se probará con el siguiente plan, hasta que se pueda introducir en uno o no queden mas planes.
	- Presentar los planes de estudio de la lista.
### Grupo 5
En una empresa de alquiler de coches se tienen una serie de reservas, pero a cada reserva solo  le pueden asignar coches que coincidan con lo pedido, coches de tipo básico, superior o premium.

Para la realización del ejercicio propuesto se deben crear las siguientes clases:
 - `Coche`: Identificará a un coche disponible por medio de un número de identificación. También tendrá asociado un `TipoReserva` que identifica la gama del coche. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
 - `Reserva`: Identificará a una reserva de alquiler por medio de un número de identificación y contará con una propiedad que indique la gama de coche reservada y cuantos coches son necesarios. Solo se le podrán asignar coches que coincidan con la gama elegida y sin sobrepasar los coches solicitados.
	 - Hay que definir el constructor y los métodos de acceso correspondientes.
	 - Definir el método `toString()` para mostrar los datos de la reserva y los coches ya asignados.
 - `Hilo Principal`:
	 - Generar una lista de 10 reservas con un tipo de reserva aleatorio y con un valor aleatorio de entre 1 y 3 coches solicitados.
	 - Generar 20 coches:
		 - Generar un valor de construcción aleatorio entre 0 y 100.
		 - Seleccionar la gama del coche correspondiente con el valor de construcción generado.
	 - Asignar cada coche a una de las reservas que acepte la gama del coche sin exceder el número de coches solicitados. Intentando repartir los coches por todas las reservas de forma equitativa.
	 - Presentar las reservas de la empresa de alquiler.
### Grupo 6
En una competición de eSports los jugadores se asignan a diferentes equipos por orden de llegada.

Para la realización del ejercicio propuesto se deben crear las siguientes clases:
- `Jugador`: Identificará a un jugador por medio de un número de identificación. También tendrá asociado un `RolJugador` para su estilo de juego. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
- `Equipo`: Identificará a un equipo por medio de un número de identificación. Como máximo puede tener dos jugadores con el mismo rol y como mínimo dispondrá de un atacante. Y se considerará equipo completo si tiene cuatro jugadores.
	- Hay que definir el constructor y los métodos de acceso correspondientes.
	- Definir el método `toString()` para mostrar los datos del equipo con los datos de cada jugador, y el método `isCompleto` que indica si el equipo está correctamente formado.
- `Hilo Principal`:
	- Generar una lista de 10 equipos.
	- Generar 40 jugadores:
		- Generar un valor de construcción aleatorio entre 0 y 100.
		- Seleccionar el tipo de rol correspondiente para el jugador con el valor de construcción generado.
	- Asignar cada jugador a uno de los equipos, si el jugador no puede ser asignado se probará con el siguiente equipo, hasta que se pueda introducir en uno o no queden mas equipos.
	- Presentar los equipos de la lista.
